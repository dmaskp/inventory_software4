﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Contoh
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Contoh))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.BTN_Hapus = New DevExpress.XtraEditors.SimpleButton()
        Me.BTN_Baru = New DevExpress.XtraEditors.SimpleButton()
        Me.BTN_Ubah = New DevExpress.XtraEditors.SimpleButton()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.BTN_Simpan1 = New DevExpress.XtraEditors.SimpleButton()
        Me.BTN_Hapus1 = New DevExpress.XtraEditors.SimpleButton()
        Me.BTN_Tutup1 = New DevExpress.XtraEditors.SimpleButton()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.BTN_Hapus)
        Me.GroupBox1.Controls.Add(Me.BTN_Baru)
        Me.GroupBox1.Controls.Add(Me.BTN_Ubah)
        Me.GroupBox1.Location = New System.Drawing.Point(72, 75)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox1.Size = New System.Drawing.Size(315, 142)
        Me.GroupBox1.TabIndex = 38
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Button Master"
        '
        'BTN_Hapus
        '
        Me.BTN_Hapus.Image = CType(resources.GetObject("BTN_Hapus.Image"), System.Drawing.Image)
        Me.BTN_Hapus.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.BTN_Hapus.Location = New System.Drawing.Point(197, 42)
        Me.BTN_Hapus.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BTN_Hapus.Name = "BTN_Hapus"
        Me.BTN_Hapus.Size = New System.Drawing.Size(59, 68)
        Me.BTN_Hapus.TabIndex = 26
        Me.BTN_Hapus.Text = "Hapus"
        '
        'BTN_Baru
        '
        Me.BTN_Baru.Image = CType(resources.GetObject("BTN_Baru.Image"), System.Drawing.Image)
        Me.BTN_Baru.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.BTN_Baru.Location = New System.Drawing.Point(64, 42)
        Me.BTN_Baru.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BTN_Baru.Name = "BTN_Baru"
        Me.BTN_Baru.Size = New System.Drawing.Size(59, 68)
        Me.BTN_Baru.TabIndex = 24
        Me.BTN_Baru.Text = "Baru"
        '
        'BTN_Ubah
        '
        Me.BTN_Ubah.Image = CType(resources.GetObject("BTN_Ubah.Image"), System.Drawing.Image)
        Me.BTN_Ubah.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.BTN_Ubah.Location = New System.Drawing.Point(131, 42)
        Me.BTN_Ubah.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BTN_Ubah.Name = "BTN_Ubah"
        Me.BTN_Ubah.Size = New System.Drawing.Size(59, 68)
        Me.BTN_Ubah.TabIndex = 25
        Me.BTN_Ubah.Text = "Ubah"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.BTN_Simpan1)
        Me.GroupBox2.Controls.Add(Me.BTN_Hapus1)
        Me.GroupBox2.Controls.Add(Me.BTN_Tutup1)
        Me.GroupBox2.Location = New System.Drawing.Point(492, 75)
        Me.GroupBox2.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.GroupBox2.Size = New System.Drawing.Size(315, 142)
        Me.GroupBox2.TabIndex = 39
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Button Pop Input"
        '
        'BTN_Simpan1
        '
        Me.BTN_Simpan1.Image = CType(resources.GetObject("BTN_Simpan1.Image"), System.Drawing.Image)
        Me.BTN_Simpan1.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.BTN_Simpan1.Location = New System.Drawing.Point(203, 42)
        Me.BTN_Simpan1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BTN_Simpan1.Name = "BTN_Simpan1"
        Me.BTN_Simpan1.Size = New System.Drawing.Size(59, 68)
        Me.BTN_Simpan1.TabIndex = 34
        Me.BTN_Simpan1.Text = "Simpan"
        '
        'BTN_Hapus1
        '
        Me.BTN_Hapus1.Image = CType(resources.GetObject("BTN_Hapus1.Image"), System.Drawing.Image)
        Me.BTN_Hapus1.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.BTN_Hapus1.Location = New System.Drawing.Point(136, 42)
        Me.BTN_Hapus1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BTN_Hapus1.Name = "BTN_Hapus1"
        Me.BTN_Hapus1.Size = New System.Drawing.Size(59, 68)
        Me.BTN_Hapus1.TabIndex = 36
        Me.BTN_Hapus1.Text = "Hapus"
        '
        'BTN_Tutup1
        '
        Me.BTN_Tutup1.Image = CType(resources.GetObject("BTN_Tutup1.Image"), System.Drawing.Image)
        Me.BTN_Tutup1.ImageLocation = DevExpress.XtraEditors.ImageLocation.TopCenter
        Me.BTN_Tutup1.Location = New System.Drawing.Point(69, 42)
        Me.BTN_Tutup1.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.BTN_Tutup1.Name = "BTN_Tutup1"
        Me.BTN_Tutup1.Size = New System.Drawing.Size(59, 68)
        Me.BTN_Tutup1.TabIndex = 35
        Me.BTN_Tutup1.Text = "Tutup"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(16, 487)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 17)
        Me.Label1.TabIndex = 40
        Me.Label1.Text = "Label1"
        '
        'Contoh
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 16.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(913, 514)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.GroupBox2)
        Me.Margin = New System.Windows.Forms.Padding(4, 4, 4, 4)
        Me.Name = "Contoh"
        Me.Text = "Contoh"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents BTN_Hapus As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BTN_Baru As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BTN_Ubah As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents BTN_Simpan1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BTN_Hapus1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents BTN_Tutup1 As DevExpress.XtraEditors.SimpleButton
    Friend WithEvents Label1 As Label
End Class
